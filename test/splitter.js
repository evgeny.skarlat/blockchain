var Splitter = artifacts.require("./Splitter.sol");

contract('Splitter', accounts => {

	it('should throw an exception', () => {
		let instance;
		return Splitter.deployed()
			.then(_instance => {
				instance = _instance;
				return instance.withdraw({ from: accounts[0] });
			})
			.then( () => {
				assert.fail('throw');
			})
			.catch(error => {
				assert.isTrue(error.message.includes('revert'));
			}) 	
	});

	it('should be send money in a 50/50 split if odd', () => {
		let instance;

		const valueSend = 21;
		const half = (valueSend -1) / 2;

		return Splitter.deployed()
			.then(_instance => {
				instance = _instance;
				return instance.splitt(
					accounts[1],
					accounts[2], 
					{ from: accounts[0], value: valueSend}
				)
			})
			.then(result => {
				return instance.balances(accounts[1]);
			})
			.then(balance1=> {
				assert.equal(balance1, half, "account 1 balance is not half of amount sent");
				return instance.balances(accounts[2]);
			})
			.then(balance2=> {
				assert.equal(balance2, half, "account 2 balance is not half of amount sent");
			})
	});

	it('should be send money in a 50/50 split if even', () => {
		let instance;

		const valueSend = 20;
		const half = 10;

		return Splitter.deployed()
			.then(_instance => {
				instance = _instance;
				return instance.splitt(
					accounts[1],
					accounts[2], 
					{ from: accounts[0], value: valueSend}
				)
			})
			.then(result => {
				return instance.balances(accounts[1]);
			})
			.then(balance1=> {
				assert.equal(balance1, half, "account 1 balance is not half of amount sent");
				return instance.balances(accounts[2]);
			})
			.then(balance2=> {
				assert.equal(balance2, half, "account 2 balance is not half of amount sent");
			})
	});

	
});