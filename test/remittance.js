var Remittance = artifacts.require("./Remittance.sol");

contract('Remittance', accounts => {

	it('dont sent deposit for self', () => {
		let instance;

		return Remittance.deployed()
			.then(_instance => {
				instance = _instance;

				return instance.deposit(
					accounts[0],
					123, 
					456,
					23,
					{from: accounts[0], value: 30}
				)
			})
			.catch(error => {
				console.log(error);
				assert.isTrue(error.message.includes('revert'));
			}) 	
	});

	it('dont sent deposit with empty withdrawer', () => {
		let instance;

		return Remittance.deployed()
			.then(_instance => {
				instance = _instance;

				return instance.deposit(
					0,
					123, 
					456,
					23,
					{from: accounts[0], value: 30}
				)
			})
			.catch(error => {
				assert.isTrue(error.message.includes('revert'));
			}) 	
	});

	it('test deposit and refund', () => {
		let instance;

		return Remittance.deployed()
			.then(_instance => {
				instance = _instance;

				return instance.deposit(
					accounts[1],
					123, 
					456,
					23,
					{from: accounts[0], value: 30}
				)
			})
			.then(result => {
				return instance.refund(
					accounts[1],
					123,
					456,
					{ from: accounts[0] }
				)
			})
			.then(result => {
				let res = result;
				assert.equal(res.receipt.status, 1);
			})
			
	});

	it('test deposit and withdraw', () => {
		return Remittance.deployed()
			.then(_instance => {
				instance = _instance;

				return instance.deposit(
					accounts[1],
					123, 
					456,
					23,
					{ from: accounts[0], value: 30 }
				)
			})
			.then(result => {
				return instance.withdraw(
					123,
					456,
					{ from: accounts[1] }
				)
			})
			.then(result => {
				let res = result;
				assert.equal(res.receipt.status, 1);
			})
	});
});