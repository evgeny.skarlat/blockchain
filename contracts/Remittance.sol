pragma solidity ^0.4.0;

contract Remittance {
    
    address owner;
    
    struct WithdrawStruct {
        uint amount;
        uint deadline;
        address sender;
    }
    
    mapping (bytes32 => WithdrawStruct) withdrawInfos;
    
    function Remittance() {
        owner = msg.sender;
    }
    
    function deposit(address withdrawer, bytes32 password1, bytes32 password2, uint deadline)
        public
        payable
        returns(bool success)
    {
        require(withdrawer != 0 || withdrawer != msg.sender);
        
        bytes32 hash = keccak256(withdrawer, password1, password2);
        
        require(withdrawInfos[hash].amount == 0);
        
        WithdrawStruct memory withdrawInfo;
        
        withdrawInfo.amount = msg.value;
        withdrawInfo.sender = msg.sender;
        withdrawInfo.deadline = block.number + deadline;
        
        withdrawInfos[hash] = withdrawInfo;
        
        return true;
    }
    
    function refund(address destination, bytes32 password1, bytes32 password2) 
        public 
        returns(bool success)
    {
        bytes32 hash = keccak256(destination, password1, password2);
        
        WithdrawStruct withdrawInfo = withdrawInfos[hash];
        
        require(withdrawInfo.amount > 0);
        require(withdrawInfo.sender == msg.sender);
        require(withdrawInfo.deadline > block.number);
        
        uint balance = withdrawInfo.amount;
        
        withdrawInfo.amount = 0;
        withdrawInfo.deadline = 0;
        withdrawInfos[hash] = withdrawInfo;
        
        msg.sender.transfer(balance);
        
        return true;
    }
    
    
    function withdraw (bytes32 password1, bytes32 password2) 
        public
        returns(bool success)
    {
        bytes32 hash = keccak256(msg.sender, password1, password2);
        
        WithdrawStruct withdrawInfo = withdrawInfos[hash];
        
        require(withdrawInfo.deadline > block.number);
        require(withdrawInfo.amount > 0);
        
        uint amount = withdrawInfo.amount;
        
        withdrawInfo.amount = 0;
        withdrawInfo.deadline = 0;
        withdrawInfos[hash] = withdrawInfo;
        
        msg.sender.transfer(amount);
        
        return true;
    }
    
    function kill() {
        require(owner == msg.sender);
        selfdestruct(owner);
    }
}