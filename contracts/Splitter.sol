pragma solidity ^0.4.4;

contract Splitter {
    
    mapping (address => uint ) public balances;
    
    event LogMoney(
        address owner,
        address recipientOne,
        address recipientTwo,
        uint amount
    );
    
    function splitt(address recipientOne, address recipientTwo) payable returns(bool success)
    {
        
        require(msg.value > 0);
        uint totalSenderAmount  = msg.value;
        
        if (totalSenderAmount % 2 != 0) {
            totalSenderAmount -= 1;
            balances[msg.sender] += 1;
        }
        
        uint amountToSend = totalSenderAmount / 2;
        
        balances[recipientOne] += amountToSend;
        balances[recipientTwo] += amountToSend;
        
        recipientOne.transfer(balances[recipientOne]);
        recipientTwo.transfer(balances[recipientTwo]);
        
        LogMoney(
            msg.sender, 
            recipientOne, 
            recipientTwo, 
            amountToSend
        );
        
        return true;
    }
    
    function withdraw() returns(bool success)
    {
        require(balances[msg.sender] > 0);
        
        uint balance = balances[msg.sender];
        
        balances[msg.sender] = 0;
        
        msg.sender.transfer(balance);
        
        return true;
    }
    
    function kill()
        returns (bool success) 
    {
        selfdestruct(msg.sender);
        return true;
    }
    
    function() payable 
    {
        balances[msg.sender] += msg.value;
    }
}